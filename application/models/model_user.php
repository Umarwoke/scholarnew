<?php
class Model_user extends CI_Model{
 
 function __construct(){
	 parent::__construct();
	 $this->load->database();
         // Berfungsi untuk memanggil database
 }
 
 // Berfungsi untuk mengambil data pada tabel user yang ada di database kita
public function cek_data($data){ 
		$query = $this->db->get_where('users', $data);
		return $query->row();
		}

public function tambah_member($data) {
        $query = $this->db->insert('users', $data);
        return $query;
      }

public function ambil_user(){
        $query = $this->db->get('users');
        $hasil = $query->result();
        return $hasil;
    }

public function tampil_user_id($data){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $data);
        $query = $this->db->get();
        $hasil = $query->result();
        return $hasil;
    }

public function update_user($id, $data){
	$this->db->where('id',$id);
    $this->db->update('users', $data);
    }

public function delete_user($id){
    $this->db->where('id', $id);
    $this->db->delete('users');
    }
public function getCurrentRow() {
        $this->db->SELECT('*');
        $this->db->from('users');
        $query = $this->db->get();
        return $query->num_rows();
    }

public function cekusername($id){
         $cekuser = "SELECT username from users where id ='".$id."'";
        //2
        $query1 = $this->db->query($cekuser);
        return $query1->result();
    }
    

}

?>