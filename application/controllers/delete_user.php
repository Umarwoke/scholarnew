
<?php
class Delete_user extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_user');
	}

	public function index(){
		$this->load->view('admin/delete');
	}

	public function tampil_user() {
		$id = $this->uri->segment(3);
		$data['user'] = $this->model_user->ambil_user();
		$data['single_member'] = $this->model_user->tampil_user_id($id);
		//var_dump($data['single_member']); die();
		$this->load->view('admin/delete', $data);
	}

	public function delete_user() {
		$id= $this->uri->segment(3);
		$this->model_user->delete_user($id);
		$this->tampil_user();
	}
}
?>