<?php if(!defined('BASEPATH')) exit('No direct access allowed');

class Home extends CI_Controller {

    public function index() {
    $data['title'] = "Scholarplus";
    $this->load->view('wrapper', $data);
    }

}