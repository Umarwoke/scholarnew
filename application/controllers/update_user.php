
<?php
class Update_user extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_user');
		$this->load->helper('form');
        $this->load->library('form_validation');
	}

	public function index(){
		$this->load->view('admin/update');
	}

	public function tampil_user() {
		$id = $this->uri->segment(3);
		$data['user'] = $this->model_user->ambil_user();
		$data['single_member'] = $this->model_user->tampil_user_id($id);
		//var_dump($data['single_member']); die();

		$this->load->view('admin/update', $data);
	}

	public function update_user() {
		

		$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[users.username]');
       
        if ($this->form_validation->run() == FALSE)
        {
           redirect('update_user/tampil_user');
        }

        else
        {
        $id= $this->input->post('id');
		$data = array(
		'username' => $this->input->post('username')
		);
		$this->model_user->update_user($id,$data);
		redirect('update_user/tampil_user');
	}
	}
}
?>