<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_user');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        $this->load->view('auth/register');
    }

    public function daftar()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[15]|is_unique[users.username]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');

        $config['upload_path']          = 'uploads';
        $config['file_path']            = 'uploads';
        $config['full_path']            = 'uploads';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);


        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('auth/register');
        }

        else
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $passconf = $this->input->post('passconf');
            $gambar = $this->input->post('gambar');

            $data = array(
                'username' => $username,
                'password' => $password,
                'gambar' => $gambar
                );

            $query = $this->model_user->cek_data($data);

                $data = array(
                    'username' => $username,
                    'password' => sha1($password),
                    'level'    => $this->input->post('level'),
                    'gambar' => $gambar 
                );

                if ($this->model_user->tambah_member($data)){

                    redirect('login');
                }

                else{
                    redirect('register');
                }
  
        }
        
    }
    
}