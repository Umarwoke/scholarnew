<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('model_user');
    }
	
	public function index()
	{
		$this->load->view('auth/login');
        }
		
	

	public function cek_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$data = array(
				'username'=> $username,
				'password' => sha1($password)
			);

		$user = $this->model_user->cek_data($data);

		if( isset($user)){
			$data = array(
				'username'=>$user->username,
				'level'=>$user->level
			);

			$this->session->set_userdata($data);
			$level = $this->session->userdata('level');

			if($level=="admin"){
                
                redirect('admin');                        
                        
            }
			else{

				redirect('home');
            
            }
         
         }
		
		else{
			redirect('login');
		}
        }	
	

	public function logout() {
        $this->session->unset_userdata('id');
		$this->session->unset_userdata('username');
        $this->session->unset_userdata('level');
		$this->session->sess_destroy();
        redirect('login');}
	 

}