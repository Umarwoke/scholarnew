<?php
/**
 * Part of ci-phpunit-test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class User_test extends TestCase
{
    public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->model('model_user');
        $this->obj = $this->CI->model_user;
        $this->form_validation = new CI_Form_validation();
    }
    
        public function test_postskenario1(){        
        $awal = $this->obj->getCurrentRow();
        //$hasil = $this->obj->setdata($data);
        $output = $this->request('POST', ['register', 'daftar'],
            [
                'username' => 'cobauplod',
                'password' => 'cobauplod',
                'passconf' => 'cobauplod',
                'level' => 'member',
                'gambar' => 'logo.png'
            ]
        );
        $akhir = $this->obj->getCurrentRow();
        $expected = $akhir - $awal;
        //$this->assertTrue($hasil);
        $this->assertEquals(1, $expected);  
        }

        public function test_postskenario7(){        

        $output = $this->request('POST', ['register', 'daftar'],
            [
                'username' => 'cobauploa',
                'password' => 'cobauploa',
                'passconf' => 'cobauploa',
                'level' => 'member',
                'gambar' => 'web-logo1.png'
            ]
        );
        
            $this->assertRedirect('login');
        }    
        
        public function test_deletecontroller1(){        
        $awal = $this->obj->getCurrentRow();
        $output = $this->request(
            'POST',
            'delete_user/delete_user/56'
        );
        $akhir = $this->obj->getCurrentRow();
        $expected = $awal - $akhir;
        $this->assertEquals(1, $expected);  
        }
        
        public function test_update(){
            $output = $this->request('POST',['update_user','update_user'],[
                'id' => '34',
                'username' => 'malahtua20',
                'level' => 'member'
            ]);
            
            $expected = 'malahtua20';
            $actual = $this->obj->cekusername(34);
            $actual1 = $actual['0']->username;
            $this->assertEquals($actual1, $expected);  
          
        }
        
        public function test_update2(){
            $output = $this->request('POST',['update_user','update_user'],[
                'id' => '34',
                'username' => 'umarwoke',
                'level' => 'member'
            ]);
            
            $expected = 'umarwoke';
            $actual = $this->obj->cekusername(34);
            $actual1 = $actual['0']->username;
            $this->assertRedirect('update_user/tampil_user');  
          
        }
        
        public function test_update3(){
            $output = $this->request('POST',['update_user','update_user'],[
                'id' => '34',
                'username' => '',
                'level' => 'member'
            ]);
            
            $expected = '';
            $actual = $this->obj->cekusername(34);
            $actual1 = $actual['0']->username;
            $this->assertRedirect('update_user/tampil_user');  
          
        }
        
        public function test_index_login()
	{
		$output = $this->request('GET', 'login/index');
		$this->assertContains('<label>Username</label>', $output);
	}
        
        public function test_index_register()
	{
		$output = $this->request('GET', 'register/index');
		$this->assertContains('<label>Confirmation Password</label>', $output);
	}
        
        public function test_index_admin()
	{
		$output = $this->request('GET', 'admin/index');
		$this->assertContains('<strong>Admin</strong>', $output);
	}
        
        public function test_index_delete()
	{
		$output = $this->request('GET', 'delete_user/index');
		$this->assertContains('<h1>Delete Users </h1>', $output);
	}
        
        public function test_index_update()
	{
		$output = $this->request('GET', 'update_user/index');
		$this->assertContains('<h1>Update Users </h1>', $output);
	}
        
        
        
        public function test_login_admin() {

        $this->request(
                'POST', ['login', 'cek_login'], [
            'username' => 'admin',
            'password' => 'admin'
                ]
        );
        $this->assertRedirect('admin');
        }

        public function test_login_scholar() {

        $this->request(
                'POST', ['login', 'cek_login'], [
            'username' => 'pertamina',
            'password' => 'pertamina'
                ]
        );
        $this->assertRedirect('home');
        }

        public function test_login_member() {

        $this->request(
                'POST', ['login', 'cek_login'], [
            'username' => 'umarwoke',
            'password' => 'umarwoke'
                ]
        );
        $this->assertRedirect('home');
        }

        public function test_login_salah() {

        $this->request(
                'POST', ['login', 'cek_login'], [
            'username' => 'umarwoke',
            'password' => 'asdasdasdasdasd'
                ]
        );
        $this->assertRedirect('login');
        }


        public function test_login_salah1() {

        $this->request(
                'POST', ['login', 'cek_login'], [
            'username' => 'umarwoke',
            'password' => ''
                ]
        );
        $this->assertRedirect('login');
        }

        public function test_login_salah2() {

        $this->request(
                'POST', ['login', 'cek_login'], [
            'username' => '',
            'password' => 'umarwoke'
                ]
        );
        $this->assertRedirect('login');
        }
    
        public function test_login_salah3() {

        $this->request(
                'POST', ['login', 'cek_login'], [
            'username' => '',
            'password' => ''
                ]
        );
        $this->assertRedirect('login');
        }
        
       public function test_login_salah4() {

        $this->request(
                'POST', ['login', 'cek_login'], [
            'username' => 'asdasdasdsa',
            'password' => 'umarwoke'
                ]
        );
        $this->assertRedirect('login');
        }
        public function test_login_salah5() {

        $this->request(
                'POST', ['login', 'cek_login'], [
            'username' => 'sadsadsad',
            'password' => 'sdasdasdasda'
                ]
        );
        $this->assertRedirect('login');
        }

        public function test_logout() {
        $this->request('GET', 'login/logout');
        $this->assertRedirect('login');
        }

    
         public function test_logout2(){
        $_SESSION['username'] = "umarwoke";
        $_SESSION['level'] = "member";
        $this->assertTrue( isset($_SESSION['username']) );
        $this->request('GET', 'login/logout');
        $this->assertRedirect('login');
        $this->assertFalse( isset($_SESSION['username']) );
        }
    
        public function test_logout3(){
        $this->assertFalse( isset($_SESSION['username']) );
        $this->request('GET', 'login/logout');
        $this->assertRedirect('login');
        $this->assertFalse( isset($_SESSION['username']) );
        }
        
        public function test_postskenario2(){        
        $awal = $this->obj->getCurrentRow();
        //$hasil = $this->obj->setdata($data);
        $output = $this->request('POST', ['register', 'daftar'],
            [
                'username' => 'umarwoke',
                'password' => 'umarwoke',
                'passconf' => 'umarwoke',
                'level' => 'member',
                'gambar' => 'logo.png'
                
            ]
        );
        $akhir = $this->obj->getCurrentRow();
        $expected = $akhir - $awal;
        //$this->assertTrue($hasil);
        $this->assertEquals(0, $expected);  
        }

        public function test_postskenario3(){        
        $awal = $this->obj->getCurrentRow();
        //$hasil = $this->obj->setdata($data);
        $output = $this->request('POST', ['register', 'daftar'],
            [
                'username' => 'wakawaka123',
                'password' => 'wakawaka123',
                'passconf' => '',
                'gambar' => 'logo.png'
                
            ]
        );
        $akhir = $this->obj->getCurrentRow();
        $expected = $akhir - $awal;
        //$this->assertTrue($hasil);
        $this->assertEquals(0, $expected);  
        }

        public function test_postskenario4(){        
        $awal = $this->obj->getCurrentRow();
        //$hasil = $this->obj->setdata($data);
        $output = $this->request('POST', ['register', 'daftar'],
            [
                'username' => 'wakawaka123',
                'password' => '',
                'passconf' => 'wakawaka123',
                'level' => 'member',
                'gambar' => 'logo.png'
                
            ]
        );
        $akhir = $this->obj->getCurrentRow();
        $expected = $akhir - $awal;
        //$this->assertTrue($hasil);
        $this->assertEquals(0, $expected);  
        }

        public function test_postskenario5(){        
        $awal = $this->obj->getCurrentRow();
        //$hasil = $this->obj->setdata($data);
        $output = $this->request('POST', ['register', 'daftar'],
            [
                'username' => '',
                'password' => 'wakawaka123',
                'passconf' => 'wakawaka123',
                'level' => 'member',
                'gambar' => 'logo.png'
            ]
        );
        $akhir = $this->obj->getCurrentRow();
        $expected = $akhir - $awal;
        //$this->assertTrue($hasil);
        $this->assertEquals(0, $expected);  
        }
        
        public function test_deletecontroller2(){        
        $awal = $this->obj->getCurrentRow();
        $output = $this->request(
            'POST',
            'delete_user/delete_user/1'
        );
        $akhir = $this->obj->getCurrentRow();
        $expected = $awal - $akhir;
        $this->assertEquals(0, $expected);  
        }
        
        public function test_deletecontroller3(){        
        $awal = $this->obj->getCurrentRow();
        $output = $this->request(
            'POST',
            'delete_user/delete_user'
        );
        $akhir = $this->obj->getCurrentRow();
        $expected = $awal - $akhir;
        $this->assertEquals(0, $expected);  
        }
        
        public function test_tampil(){
        $output = $this->request('GET', 'delete_user/tampil_user/28');
        $this->assertContains('<h5>User Detail</h5>', $output);
        }
        
        public function test_tampil2(){
        $output = $this->request('GET', 'update_user/tampil_user/20');
        $this->assertContains('<h1>Update Users </h1>', $output);
        }
        
        
    
         public function test_submit_masuk_nopassword(){
        $this->request('POST', 'login/cek_login',
            [
                'username' => 'umarwoke',
                'password' => '',
            ]);
        $this->assertRedirect('login');
        $this->assertFalse( isset($_SESSION['username']) );
        }
    
        public function test_submit_masuk_nousername(){
        $this->request('POST', 'login/cek_login',
            [
                'username' => '',
                'password' => 'umarwoke',
            ]);
        $this->assertRedirect('login');
        $this->assertFalse( isset($_SESSION['username']) );
        }
    
        public function test_submit_masuk_unmatch(){
        $this->request('POST', 'login/cek_login',
            [
                'username' => 'umarwoke',
                'password' => 'ngawur',
            ]);
        $this->assertRedirect('login');
        $this->assertFalse( isset($_SESSION['username']) );
        }
    
        public function test_submit_masuk(){
        $this->assertFalse( isset($_SESSION['username']) );
        $this->request('POST', 'login/cek_login',
            [
                'username' => 'umarwoke',
                'password' => 'umarwoke',
            ]);
        $this->assertRedirect('home');
        $this->assertEquals('umarwoke', $_SESSION['username']);
        }
    
        public function test_keluar(){
        $_SESSION['username'] = "umarwoke";
        $_SESSION['level'] = "member";
        $this->assertTrue( isset($_SESSION['username']) );
        $this->request('GET', 'login/logout');
        $this->assertRedirect('login');
        $this->assertFalse( isset($_SESSION['username']) );
        }
    
        public function test_keluar_1(){
        $this->assertFalse( isset($_SESSION['username']) );
        $this->request('GET', 'login/logout');
        $this->assertRedirect('login');
        $this->assertFalse( isset($_SESSION['username']) );
        }
    
        public function test_method_404()
        {
        $this->request('GET', 'home/method_not_exist');
        $this->assertResponseCode(404);
        }

        public function test_APPPATH()
        {
        $actual = realpath(APPPATH);
        $expected = realpath(__DIR__ . '/../..');
        $this->assertEquals(
            $expected,
            $actual,
            'Your APPPATH seems to be wrong. Check your $application_folder in tests/Bootstrap.php'
        );
        }
}
