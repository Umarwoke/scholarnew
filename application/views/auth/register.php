<?php $this->load->view('wrapper'); ?>  

	<div class="vertical-center">
		<div class="row">
			<div class="center-align">
            	<br>
            	<?php echo validation_errors(); ?>
            	<?php echo form_open('register/daftar'); ?>
            	<div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Username</label>
                                <input type="text" 
                                       name="username" 
                                       class="form-control"  
                                       id="name">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Password</label>
                                <input type="password" 
                                       name="password" 
                                       class="form-control"  
                                       id="password">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Confirmation Password</label>
                                <input type="password" 
                                       name="passconf" 
                                       class="form-control"  
                                       id="password">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                          <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Gambar</label>
                                <?php echo form_open_multipart('register/do_upload');?>
                                <input type="file" 
                                       name="gambar" 
                                       class="form-control"  
                                       id="name"
                                       size="20">
                          </div>
                    
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <input type="hidden" 
                                       name="level" 
                                       class="form-control"  
                                       id="level"
                                       value="member">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <br>
                        <div id="success"></div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <button type="submit" name="masuk" class="btn red btn-default">Register</button>
                            </div>
                        </div>
                        </form>
                            <a href="<?= site_url('login');?>">
                                <strong>Login</strong>
                            </a>
            	</div>
			</div>
		</div>
	</div>
</body>
</html>