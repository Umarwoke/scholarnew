<?php $this->load->view('wrapper'); ?>  
<!-- Main Content -->
<div class="container">
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
		   
			<!-- Contact Form - Enter your email address on line 19 of the mail/contact_me.php file to make this form work. -->
			<!-- WARNING: Some web hosts do not allow emails to be sent through forms to common mail hosts like Gmail or Yahoo. It's recommended that you use a private domain email address! -->
			<!-- NOTE: To use the contact form, your site must be on a live web host with PHP! The form will not work locally! -->
			
		</div>
	</div>
</div>


<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<section id="login">
    <div class="vertical-center">
        <div class="row">
            <div class="center-align">
                <div class="page-heading">

                    <br><br><br>
                    <?php if(isset ($fail)) { ?>
                        <p class="text-danger">Nama pengguna atau kata sandi yang dimasukkan salah</p>
                    <?php } ?>
                    <?php echo form_open('login/cek_login'); ?>	
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Username</label>
                                <input type="text" 
                                       name="username" 
                                       class="form-control" id="name" 
                                       required data-validation-required-message="Masukkan username anda!">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Password</label>
                                <input type="password" 
                                       name="password" 
                                       class="form-control" id="password" 
                                       required data-validation-required-message="Masukkan password anda!">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <button type="submit" name="masuk" class="btn red btn-default">Login</button>
                            </div>
                        </div>
                        </form>
                            <a href="<?= site_url('register');?>">
                                <strong>Register</strong>
                            </a>
                </div>
            </div>
        </div>
    </div>
<!--/header-->
</section>
<hr>