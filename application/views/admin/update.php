<?php $this->load->view('wrapper'); ?>  

<div id="container">
	<div id="wrapper">
		<h1>Update Users </h1>
		<div id="menu">
			<h5>Choose Users</h5>
			<!-- Fetching Names Of All Students From Database -->
			<ol>
				<?php foreach ($user as $u): ?>
				<li><a href="<?php echo site_url("/update_user/tampil_user/" . $u->id); ?>"><?php echo $u->username; ?></a></li>
				<?php endforeach; ?>
			</ol>
		</div>
		
		<div id="detail">
		<!-- Fetching All Details of Selected Student From Database And Showing In a Form -->
			<?php //var_dump($single_member); 
			foreach ($single_member as $member): ?>
				<h5>Edit</h5>
				<?php echo validation_errors(); ?>
				<?php echo validation_errors(); ?>

				<form method="post" action="<?php echo site_url("/update_user/update_user/"); ?>">
				<label>Username</label>
					<input type="hidden" 
						   name="id" 
						   id="id" 
						   value="<?php echo $member->id; ?>">
					<input type="text" 
                           name="username" 
                           class="form-control"  
                           id="name" 
                           value="<?php echo $member->username; ?>">
                    <p class="help-block text-danger"></p>
					<div class="row">
                            <div class="form-group col-xs-12">
                                <button type="submit" name="edit" class="btn red btn-default">Edit</button>
                            </div>
                        </div>
				</form>
			<?php endforeach; ?>
		</div>
	</div>
</div>
</body>
</html>