

<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid" 
         style="background:#FFFFFF;">
        <!-- Brand and toggle get grouped for better mobile display -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" 
             id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?= site_url('');?>" 
                       style="color:#ffffff" >
                        Home
                    </a>
                </li>
                <?php if($this->session->has_userdata('username')) {?>
                <li>
                    <a href="<?= site_url('dashboard'.$this->session->username) ?>" 
                       style="color: #ffffff;" >
                        <?=  $this->session->username; ?>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('login/logout') ?>" 
                       style="color: #ffffff;" >
                        Logout
                    </a>
                </li>
                <?php } else { ?>
                <li>
                    <a style="color: #ffffff;" href="<?= site_url('login') ?>">
                        Login
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('register') ?>" 
                       style="color: #ffffff;" >
                        Register
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>